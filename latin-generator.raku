#!/bin/env raku

#`(
	#generate the base compose file for extended latin.
	#this file still needs to be heavily edited by hand
my %specials = 
		'⎄' => 'Multi_key',
		'%' => 'percent',
		'&' => 'ampersand',
		'-' => 'minus',
		'_' => 'underscore',
		'>' => 'greater',
		'<' => 'less',
		',' => 'comma',
		'46' => 'period',
		'$' => 'dollar',
		'!' => 'exclam',
		'?' => 'question',
		'+' => 'plus',
		'/' => 'slash',
		'#' => 'numbersign',
		'@' => 'at',
		'|' => 'bar',
		'`' => 'grave',
		'~' => 'asciitilde',
		'^' => 'asciicircum',
		'(' => 'parenleft',
		')' => 'parenright',
		'[' => 'bracketleft',
		']' => 'bracketright',
		'{' => 'braceleft',
		'}' => 'braceright',
		"'" => 'apostrophe',
		'"' => 'quotedbl',
		'\\'=> 'backslash',
		':' => 'colon',
		';' => 'semicolon',
		'=' => 'equal',
		' ' => 'space',
		'*' => 'asterisk',
		'↑' => 'Up',
		'↓' => 'Down',
		'→' => 'Right',
		'←' => 'Left',
		'␣' => 'space',
		'★' => '★',
		'8725'=> 'slash',
		'768' => '<1> <grave>',
		'769' => '<1> <apostrophe>',
		'770' => '<1> <asciicircum>',
		'771' => '<1> <asciitilde>',
		'776' => '<1> <quotedbl>',
		'778' => '<1> <o>',
		'807' => '<3> <rightparen>',
		'772' => '<1> <minus>',
		'808' => '<3> <leftparen>',
		'775' => '<1> <period>',
		'780' => '<1> <v>',
		'774' => '<1> <u>',
		'779' => '<1> <2> <apostrophe>',
		'795' => '<2> <apostrophe>',
		'783' => '<1> <2> <grave>',
		'183' => '<5> <period>',
		'806' => '<3> <comma>',
		'805' => '<3> <o>',
		'813' => '<3> <asciicircum>',
		'814' => '<3> <u>',
		'803' => '<3> <period>',
		'816' => '<3> <asciitilde>',
		'785' => '<1> <O> <u>',
		'817' => '<3> <minus>',
		'804' => '<3> <2> <period>',
		'777' => '<1> <comma>',
		'702' => '<1> <greater>',
		;

"latin.compose".IO.spurt("");
for "latin-base".IO.lines -> $line {
	my $letter = $line.substr(0, 1);
	my Str $new-line = "<Multi_key> <L> ";
	for $letter.NFKD -> $chr {
		$new-line ~= (%specials{$chr}//('<'~$chr.chr~'>')) ~ " ";
	}
	$new-line ~= "\t\t\"{$letter}\"" ~ $line.substr(1) ~ "\n";
	"latin.compose".IO.spurt($new-line, :append);
}
)

# generate spaces after entries that are a sub-sequence of other entries

#first get the compose file
my @lines = "latin-base-fixed".IO.lines;
my %sequences;
for @lines -> $line {
	my $split = $line.split(":");
	my $sequence = $split[0].trim;
	my $metadata = ": "~$split[1..*].join.trim;
	%sequences{$sequence} = $metadata;
}
my %dup-sequences = %sequences;

#then compare the sequences to each other
my @sequence-keys = %sequences.keys;
SEQUENCE:
for @sequence-keys -> $sequence {
	for @sequence-keys -> $current {
		if $current ne $sequence and $current.starts-with($sequence) {
			%dup-sequences{$sequence} = "space";
			next SEQUENCE;
		}
	}
}
for @sequence-keys -> $sequence {
	if %dup-sequences{$sequence}:exists and %dup-sequences{$sequence} eq "space" {
		my $metadata = %sequences{$sequence};
		%sequences{$sequence}:delete;
		%sequences{$sequence~' <space>'} = $metadata;
	}
}
say .key~"\t"~.value for %sequences.sort(*.value);
